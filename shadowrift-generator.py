#!/usr/bin/python
import random
import cgitb
import time
import cgi
import os

seed = time.time()

if 'REQUEST_METHOD' in os.environ:
    print "Content-Type: text/html"     # HTML is following
    print                               # blank line, end of headers

    cgitb.enable()

    form = cgi.FieldStorage()
    seed = form.getfirst("seed", seed)

random.seed(seed)

cards = {
    'Actions': ["Heal", "Prophecy", "Ressurect", "Revitalize", "Rousing Speech"],
    'Loot': ["Armor of Mist", "Elixir of Life", "Lightning Daggers", "Shining Blade"],
    'Attacks': ["Blessed Smite", "Fireball", "Leading Strike", "Spear Volley", "Thieving Strike", "Wild Charge"],
    'Skills': ["Bamboozle", "Brawler", "Flanking", "Frenzy", "Holy Aura"]
}

cardlist = []
for group in cards.keys():
    for card in cards[group]:
        cardlist.append(card)

deck = random.sample(cardlist, 8)

if 'REQUEST_METHOD' in os.environ:
    print "<pre>"

for group in cards.keys():
    print group.ljust(max(len(g) for g in cards.keys())) + ":",
    print ", ".join([card for card in deck if card in cards[group]])

if 'REQUEST_METHOD' in os.environ:
    print
    print '''seed: <a href="?seed=%s">%s</a>''' % (seed, seed)
    print '''<a href="?seed=">random</a>'''


